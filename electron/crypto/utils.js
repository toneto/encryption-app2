const crypto = require('crypto');

let createHashFn = (algorithm) => {
  return (data) => {
    let hashObj = crypto.createHash(algorithm);
    hashObj.update(data);
    return hashObj.digest();
  }
};

let md5 = createHashFn('md5');

let bytesToKey = (password, salt, callback) => {
  let hash = [];

  hash[0] = md5(Buffer.concat([password, salt]));
  hash[1] = md5(Buffer.concat([hash[0], password, salt]));
  hash[2] = md5(Buffer.concat([hash[1], password, salt]));

  return [Buffer.concat([hash[0], hash[1]]), hash[2]];
}

exports.md5 = md5;
exports.bytesToKey = bytesToKey;
