const crypto = require('crypto');

const { bytesToKey } = require('./utils');

const createCipherStream = (salt, password) => {
  let [key, iv] = bytesToKey(password, salt);

  return crypto.createCipheriv('aes-256-cbc', key, iv);
}

exports.createCipherStream = createCipherStream;
