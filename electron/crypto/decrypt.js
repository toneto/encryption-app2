const crypto = require('crypto');
const { bytesToKey } = require('./utils');

const createDecipherStream = (salt, password) => {
  let [key, iv] = bytesToKey(password, salt);

  return crypto.createDecipheriv('aes-256-cbc', key, iv);
}

const decipher = (rawData, password, cb) => {
  let salt = rawData.slice(8, 16);
  let data = rawData.slice(16);
  let decrypted = Buffer.alloc(0);

  const decipherObject = createDecipherStream(salt, password);

  decipherObject.on('readable', () => {
    const data = decipherObject.read();

    if (data) {
      decrypted = Buffer.concat([decrypted, data]);
    }
  });

  decipherObject.on('end', () => {
    cb(null, decrypted);
  });

  decipherObject.on('error', (err) => {
    cb(err);
  });

  decipherObject.write(data);
  decipherObject.end();
}

exports.createDecipherStream = createDecipherStream;
exports.decipher = decipher;
