const { Duplex } = require("stream");
const { eachSeries } = require("async");

class GrepStream extends Duplex {

  constructor(regExp, options) {
    super(options);
    this.regExp = new RegExp(regExp, 'i');
    this._buf = Buffer.alloc(0);

    this.on('pipe', (src) => {
      src.on('end', () => {
        this.filterBuffer();
        this.push(null);
      });
    })
  }

  // chunk <Buffer> | <string> | <any> The chunk to be written. Will always be a buffer unless the decodeStrings option was set to false or the stream is operating in object mode.
  // encoding <string> If the chunk is a string, then encoding is the character encoding of that string. If chunk is a Buffer, or if the stream is operating in object mode, encoding may be ignored.
  // callback <Function> Call this function (optionally with an error argument) when processing is complete for the supplied chunk
  _write(chunk, encoding, callback) {
    if (typeof chunk === 'string') chunk = Buffer.from(chunk, encoding);

    this._buf = Buffer.concat([this._buf, chunk]);

    this.filterBuffer();

    return callback();
  }

  // chunks <Array> The chunks to be written. Each chunk has following format: { chunk: ..., encoding: ... }.
  // callback <Function> A callback function (optionally with an error argument) to be invoked when processing is complete for the supplied chunks.
  _writev(chunks, callback) {
    eachSeries(chunks, (item, cb) => this._write(item.chunk, item.encoding, cb), callback);
  }

  // callback <Function> Call this function (optionally with an error argument) when you are done writing any remaining data.
  _final(callback) {
    return callback();
  }

  // size <number> Number of bytes to read asynchronously
  _read(size) {
  }

  filterBuffer() {
    let stop = false;

    while (!stop && this._buf.length) {
      let i = this._buf.indexOf(10);

      // If there is no newline, return
      if (i === -1) return;

      // Else, take the first line and test it
      let chunk = this._buf.slice(0, i + 1);
      this._buf = this._buf.slice(i + 1);

      if (this.regExp.test(chunk.toString('utf8'))) {
        stop = !this.push(chunk);
      };
    }
  }
}

exports.GrepStream = GrepStream;
