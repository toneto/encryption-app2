const { Duplex } = require("stream");

class PrependSaltStream extends Duplex {

  constructor(salt, options) {
    super(options);
    this.beginning = Buffer.concat([Buffer.from('Salted__'), salt]);
    this.beginningSent = false;

    this.on('pipe', (src) => {
      src.on('end', () => this.push(null) )
    });
  }

  // chunk <Buffer> | <string> | <any> The chunk to be written. Will always be a buffer unless the decodeStrings option was set to false or the stream is operating in object mode.
  // encoding <string> If the chunk is a string, then encoding is the character encoding of that string. If chunk is a Buffer, or if the stream is operating in object mode, encoding may be ignored.
  // callback <Function> Call this function (optionally with an error argument) when processing is complete for the supplied chunk
  _write(chunk, encoding, callback) {
    if (typeof chunk === 'string') chunk = Buffer.from(chunk, encoding);

    if (!this.beginningSent) {
      this.push(this.beginning);
      this.beginningSent = true;
    }

    this.push(chunk);

    return callback();
  }

  // chunks <Array> The chunks to be written. Each chunk has following format: { chunk: ..., encoding: ... }.
  // callback <Function> A callback function (optionally with an error argument) to be invoked when processing is complete for the supplied chunks.
  _writev(chunks, callback) {
    eachSeries(chunks, (item, cb) => this._write(item.chunk, item.encoding, cb), callback);
  }

  // callback <Function> Call this function (optionally with an error argument) when you are done writing any remaining data.
  _final(callback) {

    return callback();
  }

  // size <number> Number of bytes to read asynchronously
  _read(size) {
  }
}

exports.PrependSaltStream = PrependSaltStream;
