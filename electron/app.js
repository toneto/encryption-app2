const { dialog, ipcMain } = require('electron');
const { first } = require('lodash');
const crypto = require('crypto');
const fs = require('fs');

const { createDecipherStream } = require('./crypto/decrypt');
const { createCipherStream } = require('./crypto/encrypt');
const { PrependSaltStream } = require('./utils/prepend-salt-stream');

ipcMain.on('show-open-dialog', (event) => {
  dialog.showOpenDialog({ properties: ['openFile'] }, (filePaths) => {
    event.sender.send('show-open-dialog', first(filePaths));
  });
});

ipcMain.on('show-save-dialog', (event) => {
  dialog.showSaveDialog({}, (filePath) => {
    event.sender.send('show-save-dialog', filePath);
  });
});

ipcMain.on('decrypt', (event, values) => {
  fs.open(values.input, 'r', (err, fd) => {
    if (err) return event.sender.send('decrypt', err);

    let salt = Buffer.alloc(8);

    fs.read(fd, salt, 0, 8, 8, err => {
      let decipherStream = createDecipherStream(salt, Buffer.from(values.password, 'utf8'));
      let readStream = fs.createReadStream(values.input, { start: 16 });
      let writeStream = fs.createWriteStream(values.output);

      readStream.on('error', err => event.sender.send('decrypt', err) );
      decipherStream.on('error', err => event.sender.send('decrypt', err) );
      writeStream.on('close', err => event.sender.send('decrypt', err) );

      readStream.pipe(decipherStream).pipe(writeStream);
    });
  });
});

ipcMain.on('encrypt', (event, values) => {
  let salt = crypto.randomBytes(8);
  let cipherStream = createCipherStream(salt, Buffer.from(values.password));
  let prependSaltStream = new PrependSaltStream(salt);
  let readStream = fs.createReadStream(values.input);
  let writeStream = fs.createWriteStream(values.output);

  prependSaltStream.on('error', err => event.sender.send('encrypt', err) );
  writeStream.on('error', err => event.sender.send('encrypt', err) );
  cipherStream.on('error', err => event.sender.send('encrypt', err) );
  writeStream.on('close', err => event.sender.send('encrypt', err) );

  readStream.pipe(cipherStream).pipe(prependSaltStream).pipe(writeStream);
});
