import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DecryptComponent } from './decrypt/decrypt.component';
import { EncryptComponent } from './encrypt/encrypt.component';

const routes: Routes = [
  { path: 'decrypt', component: DecryptComponent, data: { animation: 'DecryptPage' } },
  { path: 'encrypt', component: EncryptComponent, data: { animation: 'EncryptPage' } },
  { path: '**', redirectTo: '/encrypt', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
