import { Injectable, NgZone } from '@angular/core';

import { ExecutorService } from '../executor/executor.service';

declare let electron: any;

@Injectable()
export class IpcService{
  ipc: any;

  constructor(private zone: NgZone, private executorService: ExecutorService) {
    this.ipc = electron.ipcRenderer;
  }

  getIpcRenderer(): any {
    return this.ipc;
  }

  onZone(channel: string, cb: GenericFunction) {
    return this.ipc.on(channel, (...args: any[]) => {
      this.zone.run( () => cb.apply(undefined, args) );
    });
  }

  onceZone(channel: string, cb: GenericFunction) {
    return this.ipc.once(channel, (...args: any[]) => {
      this.zone.run( () => cb.apply(undefined, args) );
    });
  }

  push(channel: string, ipcArgs: any[], ipcCallback: GenericFunction) {
    ipcArgs.unshift(channel);

    let task: ExecutorTask = {
      args: [],
      fn: (executorCallback: ExecutorCallback) => {
        this.ipc.once(channel, (...args: any[]) => {
          this.zone.run( () => ipcCallback.apply(undefined, args) )
          return executorCallback()
        });
        this.ipc.send.apply(null, ipcArgs);
      }
    };

    this.executorService.push(task);
  }

}
