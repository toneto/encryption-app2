import { Injectable } from '@angular/core';
import { queue } from 'async';
import * as _ from 'lodash';

let worker = (task: ExecutorTask, executorCallback: ExecutorCallback) => {
  let args = _.get(task, "args", []);
  let lastArg = _.last(args);

  if (_.isFunction(lastArg)) {
    // Wrap taskCallback
    args[args.length - 1] = function () {
      lastArg.apply(null, arguments);
      executorCallback.apply(null, arguments);
    }
  }
  else {
    args.push(executorCallback);
  }

  task.fn.apply(task.thisValue, args);
}

@Injectable()
export class ExecutorService {

  private q;

  constructor() {
    this.q = queue(worker, 1);

    this.q.error((err, task) => {
      if (err) throw err;
    });
  }

  push(task: ExecutorTask): void {
    this.q.push(task)
  }

  unshift(task: ExecutorTask) {
    this.q.unshift(task)
  }
}
