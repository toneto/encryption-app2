import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material/material.module';

import { AppComponent } from './app.component';
import { DecryptComponent } from './decrypt/decrypt.component';
import { EncryptComponent } from './encrypt/encrypt.component';

import { ExecutorService } from './executor/executor.service';
import { IpcService } from './ipc/ipc.service';

@NgModule({
  declarations: [
    AppComponent,
    DecryptComponent,
    EncryptComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  providers: [
    ExecutorService,
    IpcService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
