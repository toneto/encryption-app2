import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { MatStepper } from '@angular/material/stepper';

import { IpcService } from '../ipc/ipc.service';

@Component ({
  selector: 'decrypt',
  templateUrl: './decrypt.component.html',
  styleUrls: ['./decrypt.component.scss']
})
export class DecryptComponent {

  public form: FormGroup;
  @ViewChild(MatStepper) stepper: MatStepper;

  constructor(
    private fb: FormBuilder,
    private ipc: IpcService,
    private router: Router,
    private snackbar: MatSnackBar
  ) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      input: ['', Validators.required],
      output: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  selectInputFile() {
    this.ipc.push('show-open-dialog', [], (_event:any, filename: string) => {
      if (filename) {
        this.form.get('input').setValue(filename);
        this.stepper.next();
      }
    });
  }

  selectOutputFile() {
    this.ipc.push('show-save-dialog', [], (_event:any, filename: string) => {
      if (filename) {
        this.form.get('output').setValue(filename);
        this.stepper.next();
      }
    });
  }

  onSubmit() {
    this.ipc.push('decrypt', [this.form.value], (_event: any, err: Error) => {
      if (!err) {
        this.form.reset();
        this.snackbar.open('Terminado', 'Aceptar', { duration: 5000 });
      }
      else {
        console.log(err);
        this.snackbar.open('Error', 'Aceptar');
      }
    });
    this.snackbar.open('Desencriptando...');
  }

  swipeRight() {
    this.router.navigate(['/encrypt']);
  }
}
