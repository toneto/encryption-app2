/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

interface ExecutorTask {
  fn: (...params: any[]) => any,
  thisValue?: any,
  args?: any[],
}

interface ExecutorCallback {
  (error?: any): void,
}

interface GenericFunction {
  (...params: any[]): any
}

interface DecryptOptions {
  input: string;
  password: string;
  output?: string;
  grep?: string;
  format?: string;
}
